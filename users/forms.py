from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import get_user_model
from django import forms
from django.shortcuts import redirect
from django.contrib import messages

	

class RegisterForm(UserCreationForm):
    password2 = forms.CharField(
        label=("Konfirmasi Password"),
        strip=False,
        widget=forms.PasswordInput(attrs={'autocomplete': 'new-password'}),
        help_text="Ketikkan ulang password user",
    )
    username = forms.CharField(
        label=("Username"),
        strip=False,
        widget=forms.TextInput(attrs={'autocomplete': 'new-password'}),
        help_text=("Wajib!.150 karakter atau kurang"),
    )
    email = forms.CharField(
        label=("Alamat email"),
        strip=False,
        widget=forms.TextInput(attrs={'autocomplete': 'new-password'}),
    )
    password1 = forms.CharField(
        label=("Password"),
        strip=False,
        widget=forms.PasswordInput(attrs={'autocomplete': 'new-password'}),
        help_text= "Jangan sebarkan ke siapapun!"
    )
    class Meta:
        model = get_user_model()
        fields = ('username','email',)


 

class LoginForm(AuthenticationForm):
    username = forms.CharField(label='Email / Username')

