from django.contrib.auth import views as auth_views
from django.views import generic
from django.urls import reverse_lazy
from django.contrib import messages
from django.shortcuts import redirect,render
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from .forms import LoginForm, RegisterForm


class Login(auth_views.LoginView):
    form_class = LoginForm
    template_name = 'users/login.html'
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('home')
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self,form):
    	if self.request.user.is_authenticated:
    		return redirect('home')
    	return super().form_valid(form)


class Register(generic.CreateView,LoginRequiredMixin, UserPassesTestMixin):
    form_class = RegisterForm
    template_name = 'users/register.html'
    success_url = reverse_lazy('register')

    def form_valid(self,form):
        u_name = form.instance.username
        messages.success(self.request,f'akun: {u_name} berhasil dibuat')
        return super().form_valid(form)
    def test_func(self):
        if self.request.user.is_staff:
            return True
        return False