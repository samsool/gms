from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import gettext_lazy as _
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User

class CustomUser(AbstractUser):
    email = models.EmailField(_('email address'), unique=True)


