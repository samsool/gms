FROM ubuntu:22.04

RUN apt-get update
RUN apt -y install python3
RUN apt install -y python3-pip
RUN apt-get install -y software-properties-common
RUN apt-get update
RUN add-apt-repository ppa:ubuntugis/ppa
RUN apt-get update
RUN apt-get install -y  gdal-bin
RUN apt-get install -y  libgdal-dev
RUN export CPLUS_INCLUDE_PATH=/usr/include/gdal
RUN export C_INCLUDE_PATH=/usr/include/gdal

WORKDIR /app

COPY requirements.txt .

RUN pip install GDAL

RUN pip install -r requirements.txt

COPY . .

EXPOSE 9595

CMD ["python3", "manage.py","runserver","0.0.0.0:9595"]