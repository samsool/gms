"""WMS URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('tentang/', views.tentang,name='tentang'),
    path('', views.home.as_view(),name='home'),
    path('uploadgeo/', views.UploadDataGeo.as_view(),name='uploadGeo'),
    path('editmap/<int:pk>',views.PostUpdateView.as_view(),name='edit'),
    path('hapusmap/<int:pk>',views.PostDeleteView.as_view(),name='hapus'),
    path('detailmap/<int:pk>',views.PostDetailView.as_view(),name='detail'),
    path('setting/',views.SettingUserView.as_view(),name='settings'),
    path('setting/user/hapus/<int:pk>',views.UserDeleteView.as_view(),name='hapusUser'),
    path('yourpost',views.PostUserDetailView.as_view(),name='urpost'),





]
