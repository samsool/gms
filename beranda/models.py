from django.db import models
from django.contrib.auth.models import User
from users.models import CustomUser
import json 
import os 
from django.utils import timezone
from django.urls import reverse
from django.core.files import File
from io import BytesIO


def file_path(instance, filename):
    ext = filename.split('.')
    print(ext)
  
    if ext[1] != 'js':
    	filename = ext[0] +  ".geojson"
 

    return f'geojson/{filename}'

class mapList(models.Model):
    default_directory = "geojson"
    thumbnail = models.ImageField(default="gmbr/default.jpg",upload_to='gmbr')
    latitude = models.FloatField(default=0)
    wilayah_map = models.CharField(max_length=150)
    jenis_map = models.CharField(max_length=150)
    file = models.FileField(upload_to=file_path)
    longtitute= models.FloatField(default=0)
    date = models.DateTimeField('tanggal dataset',default = timezone.now)
    uploader = models.ForeignKey(CustomUser,on_delete = models.CASCADE)

   

    def __str__(self):
        return self.wilayah_map

    def save(self):
        super().save()
        
    def get_absolute_url(self):
        return reverse('home')

    def delete(self):
        self.file.delete(save=False)
        if self.thumbnail.name != 'gmbr/default.jpg':
            self.thumbnail.delete(save=False)
        super().delete()

class comment(models.Model):
	isi = models.TextField()
	date = models.DateTimeField('tanggal komen',default = timezone.now)
	comenter = models.ForeignKey(CustomUser,on_delete=models.CASCADE)
	section = models.ForeignKey(mapList,on_delete=models.CASCADE)

	def __str__(self):
		self.isi


	

			

