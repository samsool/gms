from django.shortcuts import render,redirect
from django.views import generic
from .models import mapList
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from users.models import CustomUser
from django.contrib import messages
from django.urls import reverse
from django.http import HttpResponse
from django.db.models import Q
import pandas as pd
import geopandas as gpd
import os
import matplotlib.pyplot as plt
from django.conf import settings
from random import random
from django.urls import reverse_lazy
from django.core.files.storage import default_storage
from io import BytesIO
from PIL import Image
import matplotlib
import urllib.request
from django.core.files.storage import FileSystemStorage
from WMS.settings import BASE_DIR,DROPBOX_ROOT_PATH
matplotlib.use('agg')
#import boto3
User = CustomUser()
'''
def s3_delete(id):
    s3conn = boto.connect_s3(settings.AWS_ACCESS_KEY_ID,settings.AWS_SECRET_ACCESS_KEY)
    bucket = s3conn.get_bucket(settings.AWS_STORAGE_BUCKET_NAME)

    k = Key(bucket)
    k.key = str(id)
    k.delete()
'''
def prosesMap(obj):
    temp =[]
    f = default_storage.open(obj.name,'r')
    temp += f.readlines()
    f.close()
    old = [b'var Data = ']
    if b'var' not in temp[0]:
        old += temp
        z = default_storage.open(obj.name,'w')
        print(old)
        for c in old:
            z.write(c)
        z.close()

def thumbnail(obj):
    print(obj.url)
        # Build paths inside the project like this: BASE_DIR / 'subdir'.
    temp = BytesIO()
    fig = plt.figure()
    df_places = gpd.read_file(f"{obj.url}",driver="GeoJSON")
    #df_places = gpd.read_file(f"{obj.name}",driver="GeoJSON")
    ax = fig.add_axes([0,0,1,1])
    ax.axis('off')
    df_places.plot(ax=ax,figsize=(3, 3))
    nama = 'gambar'+str(random()).split('.')[1]+'.png'
    plt.savefig(temp, format='png')
    gmbr = Image.open(temp,mode='r')
    ukuran = (300,300)
    gmbr.thumbnail(ukuran)
    gmbr.save(temp, 'PNG', quality=95)
    default_storage.save(f'gmbr/{nama}',temp)
    gmbr.close()
    temp.close()
    return 'gmbr/'+nama
   

'''class settingUser(generic.View):
    paginate_by = 6
    def get(self,*args,**kwargs):
        users = CustomUser.objects.all()
        posts = mapList.objects.all()
        if not self.request.user.is_staff:
            return redirect('home')
        if self.request.GET:
            
                users = CustomUser.objects.filter(Q(username__icontains= self.request.GET.get('qu'))|Q(email__icontains= self.request.GET.get('qu'))).distinct()
            elif self.request.GET.get('qp'):
                posts = mapList.objects.filter(Q(wilayah_map__icontains=self.request.GET.get('qp'))|Q(jenis_map__icontains=self.request.GET.get('qp'))).order_by('-date').distinct()
                
        context = {"users" :users,'posts':posts}
        return render(self.request,"beranda/settings.html",context)
    def post(self, *args, **kwargs):
        if self.request.POST.get('nama'):
            print(self.request.method)
            if self.request.POST.get("pk"):
                print(self.request.method)
                acc = CustomUser.objects.get(pk=self.request.POST.get("pk"))

                acc.username = self.request.POST.get("nama")
                acc.email = self.request.POST.get("email")
                if self.request.POST.get("set") == "admin":
                    acc.is_staff = True
                elif self.request.POST.get("set") == "user":
                    acc.is_staff = False
                acc.save()
                messages.success(self.request,f'akun {acc.username} berhasil diubah')
                return redirect('settings')
          
            messages.error(self.request,f'gagal')
            return redirect('settings')
           
       '''

class SettingUserView(generic.ListView):
    model = CustomUser
    template_name = "beranda/settings.html"
    context_object_name = "users"
    paginate_by = 6
    def get(self,*args,**kwargs):
        queGuest = self.request.GET.get('qp')
        if queGuest:
            posts = mapList.objects.filter(Q(wilayah_map__icontains= queGuest )|Q(jenis_map__icontains= queGuest )|Q(uploader__username__icontains= queGuest )|Q(date__year__icontains= queGuest )).order_by('-date').distinct()
            users = CustomUser.objects.all()
            context = {'posts':posts,'users':users,'totUsers':users,'totPosts':mapList.objects.all()}
            return render(self.request,"beranda/settings.html",context)
        return super().get(*args,**kwargs)

    def post(self, *args, **kwargs):
        try:
            primary_set = self.request.POST.get('pk')
            if primary_set:
                print(self.request.method)
                acc = CustomUser.objects.get(pk=primary_set)

                acc.username = self.request.POST.get("nama")
                acc.email = self.request.POST.get("email")
                if self.request.POST.get("set") == "admin":
                    acc.is_staff = True
                elif self.request.POST.get("set") == "user":
                    acc.is_staff = False
                if self.request.POST.get("password") is not None:
                    if self.request.POST.get("password") == self.request.POST.get("password2"):
                        acc.set_password(self.request.POST.get("password"))
                    else:
                        messages.warning(self.request,f'akun {acc.username} gagal diubah password tidak sama!')
                        return redirect('settings')
                acc.save()
                messages.success(self.request,f'akun {acc.username} berhasil diubah')
                return redirect('settings')
              
        except Exception as e:
            messages.warning(self.request,f'ada kesalahan ({e}) akun gagal diubah')
            return redirect('settings')

    def get_context_data(self, **kwargs):
   
        context = super().get_context_data(**kwargs)
        context['totUsers'] = CustomUser.objects.all()
        context['posts'] = mapList.objects.all()
        context['totPosts'] = mapList.objects.all()
        return context 
    def get_queryset(self):
        querry = self.request.GET.get('qu')
        object_list = self.model.objects.all().order_by('-username')
        if querry:
            object_list = self.model.objects.filter(Q(username__icontains=querry)|Q(id__icontains=querry)|Q(email__icontains=querry)).order_by('-username').distinct()
        return object_list

'''class SettingMapView(generic.ListView):
    model = mapList
    template_name = "beranda/settings.html"
    context_object_name = "posts"
    paginate_by = 6

    def get_context_data(self, **kwargs):
   
        context = super().get_context_data(**kwargs)
        context['totPosts'] = mapList.objects.all()
        return context 
    def get_queryset(self):
        querry = self.request.GET.get('qp')
        object_list = self.model.objects.all().order_by('-date')
        if querry:
            object_list = self.model.objects.filter(Q(wilayah_map__icontains=querry)|Q(jenis_map__icontains=querry)|Q(uploader__username__icontains=querry)|Q(date__year__icontains=querry)).order_by('-date').distinct()
        return object_list
'''
class UserDeleteView(LoginRequiredMixin,UserPassesTestMixin,generic.DeleteView):
    model = CustomUser
    success_url = "settings"
    template_name = "beranda/user_hapus.html"

    def delete(self, request, *args, **kwargs):
        """
        Call the delete() method on the fetched object and then redirect to the
        success URL.
        """
        self.object = self.get_object()
        success_url = self.get_success_url()
        self.object.delete()
        messages.success(self.request,f'User berhasil dihapus')
        return redirect(success_url)
    
    def test_func(self):
        postingan = self.get_object()
        if self.request.user.is_staff:
            return True
        return False



class home(generic.ListView):
    model = mapList
    template_name = "beranda/home.html"
    context_object_name = "items"
    paginate_by = 6

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['max_obj'] = mapList.objects.all().order_by('-date')
        return context 
    def get_queryset(self):
        querry = self.request.GET.get('qm')
        object_list = self.model.objects.all().order_by('-date')
        if querry: 

            object_list = self.model.objects.filter(Q(wilayah_map__icontains=querry)|Q(jenis_map__icontains=querry)|Q(uploader__username__icontains=querry)|Q(date__year__icontains=querry)).order_by('-date').distinct()

        return object_list

		

class UploadDataGeo(LoginRequiredMixin,generic.CreateView):
    model = mapList
    template_name = "beranda/upload_geojson.html"
    fields = ['wilayah_map', 'jenis_map','file','date','latitude','longtitute']
    def form_valid(self, form): 
        form.save(commit=False)
        form.instance.uploader = self.request.user
        nama = form.instance.file.name.split('.')[1]
        if nama == 'json' or nama == 'geojson' or nama == 'js':
              
            form.save()
            temp =[]
            f = default_storage.open(form.instance.file.name,'r')
            temp += f.readlines()
            f.close()
            old = [b'var Data = ']
            print('check288')
            if b'var' not in temp[0]:
                form.instance.thumbnail = thumbnail(form.instance.file)
            print('check2')
            #prosesMap(form.instance.file)
            print('check24')
            messages.success(self.request,f'tambah data wilayah: {form.instance.wilayah_map} sukses')
            super().form_valid(form)
            return redirect('uploadGeo')


  





class PostUpdateView(LoginRequiredMixin, UserPassesTestMixin,generic.UpdateView):
    model = mapList
    fields = ['wilayah_map', 'jenis_map','file','date','latitude','longtitute']
    template_name = 'beranda/upload_geojson.html'

    def form_valid(self, form):
        try:
            form.save(commit=False)
            form.instance.uploader = self.request.user
            nama = form.instance.file.name.split('.')[1]
            if nama == 'json' or nama == 'geojson' or nama == 'js':
                form.save()
                temp =[]
                f = default_storage.open(form.instance.file.name,'r')
                temp += f.readlines()
                f.close()
                old = ['var Data = ']
                if b'var' not in temp[0]:
                    form.instance.thumbnail = thumbnail(form.instance.file)
                prosesMap(form.instance.file)
                messages.success(self.request,f'tambah data wilayah: {form.instance.wilayah_map} sukses')
                super().form_valid(form)
                return redirect('detail',pk=form.instance.pk)
            messages.warning(self.request,f'gagal update, file harus json/geojson/javascript (file .{nama} tidak valid!)')
            return redirect('edit',pk=form.instance.pk)
        except Exception as e:
            messages.warning(self.request,f'Ada kesalahan {e}')
            return redirect('edit',pk=form.instance.pk)

    def test_func(self):
        postingan = self.get_object()
        if self.request.user == postingan.uploader or self.request.user.is_staff:
            return True
        return False

class PostDeleteView(LoginRequiredMixin,UserPassesTestMixin,generic.DeleteView):
    model = mapList
    success_url = "/"
    template_name = "beranda/map_hapus.html"


    def delete(self, request, *args, **kwargs):
        """
        Call the delete() method on the fetched object and then redirect to the
        success URL.
        """
  
        self.object = self.get_object()
        success_url = self.get_success_url()
        self.object.delete()
        messages.success(self.request,f'objek berhasil dihapus')
        return redirect(success_url)


    
    def test_func(self):
        postingan = self.get_object()
        if self.request.user == postingan.uploader or self.request.user.is_staff:
            return True
        return False

class PostDetailView(generic.DetailView):
    model = mapList
    template_name = "beranda/Detail_map.html"


 

class PostUserDetailView(generic.ListView):
    model = mapList
    template_name = "beranda/post-anda.html"
    context_object_name = "items"
    paginate_by = 6
    
    

    def get_queryset(self):
        querry = self.request.GET.get('qmu')
        object_list = self.model.objects.filter(uploader=self.request.user).order_by('-date')
        if querry:
            object_list = self.model.objects.filter(Q(wilayah_map__icontains=querry)|Q(jenis_map__icontains=querry)|Q(uploader__username__icontains=querry)|Q(date__year__icontains=querry)).filter(uploader=self.request.user).order_by('-date').distinct()
        return object_list


def tentang(request):
    context ={'judul':"About"}
    return render(request,"beranda/tentang.html",context)
